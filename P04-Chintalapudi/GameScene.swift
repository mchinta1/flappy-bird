//
//  GameScene.swift
//  P04-Chintalapudi
//
//  Created by MaNi on 2/28/17.
//  Copyright © 2017 MaNi. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit
import AVFoundation

struct PhysicsBody {
    static let Bird : UInt32 = 0x1 << 1
    static let Pipe : UInt32 = 0x1 << 2
    static let Floor : UInt32 = 0x1 << 3
}


class GameScene: SKScene, SKPhysicsContactDelegate {

    var GameLevel : Int = 0
    var myBackground = SKSpriteNode()
    var myFloor1 = SKSpriteNode()
    var myFloor2 = SKSpriteNode()
    var scoreLabel = SKLabelNode()
    let birdAtlas = SKTextureAtlas(named:"flying.atlas")
    var score : Int32 = 0
    var birdSprites = Array<SKTexture>()
    var bird = SKSpriteNode()
    
    var start = Bool(false)
    var birdIsActive = Bool(false)
    var pipeHeight = CGFloat(200)
    var PipeCollection = [SKSpriteNode]()
    var PipePair = SKNode()
    var PipeMoveRemove = SKAction()
    var started = Bool()
    var spawn = SKAction()
    var died = Bool(false)
    var myLabel:SKLabelNode!
   // var update_bool = true
    
    var SoundManager = AVAudioPlayer()
    
    override func didMove(to view: SKView) {
    
    
    StartScene()
    
    }
    
    
    func StartScene()
    {
        
        self.physicsWorld.contactDelegate = self;
        
        if GameLevel == 1 {
             myBackground = SKSpriteNode(imageNamed: "background0")
        }
       
        else if GameLevel == 2 {
        
            myBackground = SKSpriteNode(imageNamed: "snow_5")
        }
        
        else if GameLevel == 3 {
            
            myBackground = SKSpriteNode(imageNamed: "rock_4")
        }
        
        myBackground.anchorPoint = CGPoint(x:0,y:0)
        
        myBackground.position = CGPoint(x:0,y:0)
        
        self.backgroundColor = SKColor(red: 80.0/255.0, green: 192.0/255.0, blue: 203.0/255.0, alpha: 1.0)
        myBackground.size = self.frame.size
        
        self.addChild(self.myBackground)
        
        myFloor1 = SKSpriteNode(imageNamed: "floor")
        myFloor2 = SKSpriteNode(imageNamed: "floor")
        
        myFloor1.anchorPoint = CGPoint(x:0,y:0)
        myFloor1.position = CGPoint(x:0,y:0)
        myFloor1.size.width = (view?.frame.width)!
        myFloor1.size.height = 75
        print(myFloor1.size.height)
        
        
        started = false
        died = false
        
        myFloor2.anchorPoint = CGPoint(x:0,y:0)
        myFloor2.position = CGPoint(x:myFloor1.size.width-1, y:0)
        myFloor2.size.width = (view?.frame.width)!
        myFloor2.size.height = 75
        self.addChild(self.myFloor1)
        self.addChild(self.myFloor2)
        
        birdSprites.append(birdAtlas.textureNamed("frame-1"))
        birdSprites.append(birdAtlas.textureNamed("frame-2"))
        birdSprites.append(birdAtlas.textureNamed("frame-3"))
        birdSprites.append(birdAtlas.textureNamed("frame-4"))
        birdSprites.append(birdAtlas.textureNamed("frame-5"))
        birdSprites.append(birdAtlas.textureNamed("frame-6"))
        birdSprites.append(birdAtlas.textureNamed("frame-7"))
        birdSprites.append(birdAtlas.textureNamed("frame-8"))
        
        bird = SKSpriteNode(texture:birdSprites[0])
        bird.position = CGPoint(x:50, y:self.frame.midY+20);
        bird.size.width = 50
        bird.size.height = 60
        
        let animateBird = SKAction.animate(with:self.birdSprites, timePerFrame: 0.1)
        let repeatAction = SKAction.repeatForever(animateBird)
        bird.run(repeatAction)
        self.addChild(bird)
        
        
        bird.zPosition = 4
        myFloor1.zPosition = 3
        myFloor2.zPosition = 3
        myBackground.zPosition = 1
        
        myFloor1.physicsBody = SKPhysicsBody(rectangleOf: myFloor1.frame.size)
        myFloor1.physicsBody?.categoryBitMask = PhysicsBody.Floor
        myFloor1.physicsBody?.collisionBitMask = PhysicsBody.Bird
        myFloor1.physicsBody?.contactTestBitMask = PhysicsBody.Bird
        myFloor1.physicsBody?.affectedByGravity = false
        myFloor1.physicsBody?.isDynamic = false
        
        myFloor2.physicsBody = SKPhysicsBody(rectangleOf: myFloor2.frame.size)
        myFloor2.physicsBody?.categoryBitMask = PhysicsBody.Floor
        myFloor2.physicsBody?.collisionBitMask = PhysicsBody.Bird
        myFloor2.physicsBody?.contactTestBitMask = PhysicsBody.Bird
        myFloor2.physicsBody?.affectedByGravity = false
        myFloor2.physicsBody?.isDynamic = false
        
        bird.physicsBody = SKPhysicsBody(circleOfRadius: bird.frame.width/2)
        bird.physicsBody?.categoryBitMask = PhysicsBody.Bird
        bird.physicsBody?.collisionBitMask = PhysicsBody.Floor | PhysicsBody.Pipe
        bird.physicsBody?.contactTestBitMask = PhysicsBody.Floor | PhysicsBody.Pipe
        bird.physicsBody?.affectedByGravity = false
        bird.physicsBody?.isDynamic = true
               
    }
    
    
    func StartScoring()
    {
        scoreLabel.fontColor = UIColor.black
        scoreLabel.colorBlendFactor = 1
        scoreLabel = SKLabelNode(fontNamed: "Georgia-BoldItalic")
        scoreLabel.fontSize = 20
        scoreLabel.zPosition = 8
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scoreLabel.position = CGPoint(
            x:250,
            y:self.frame.maxY
        )
        self.addChild(scoreLabel)
        self.score = 0
        //Score incrementing by time
        let delay = SKAction.wait(forDuration: 0.25)
        let incrementScore = SKAction.run ({
            self.score = self.score + 1
            self.scoreLabel.text = "SCORE:\(self.score)"
        })
        self.run(SKAction.repeatForever(SKAction.sequence([delay,incrementScore])))

    }
    
    func createPipes()  {
        
        PipePair = SKNode()
        
        let bottomPipe1 = SKSpriteNode(imageNamed: "topPipe")
        let topPipe1 = SKSpriteNode(imageNamed: "topPipe")
        
        
        bottomPipe1.position = CGPoint(x: (self.view?.frame.width)!,y: 0);
        bottomPipe1.size.height = bottomPipe1.size.height/2
        bottomPipe1.size.width = bottomPipe1.size.width * 0.6
        bottomPipe1.zRotation = CGFloat(M_PI)
        
        
        topPipe1.position = CGPoint(x:(self.view?.frame.width)!, y:(self.view?.frame.height)! + 80 );
        topPipe1.size.height = topPipe1.size.height/2
        topPipe1.size.width = topPipe1.size.width * 0.6
        
        topPipe1.physicsBody = SKPhysicsBody(rectangleOf: topPipe1.frame.size)
        topPipe1.physicsBody?.categoryBitMask = PhysicsBody.Pipe
        topPipe1.physicsBody?.collisionBitMask = PhysicsBody.Bird
        topPipe1.physicsBody?.contactTestBitMask = PhysicsBody.Bird
        topPipe1.physicsBody?.affectedByGravity = false
        topPipe1.physicsBody?.isDynamic = false
        
        bottomPipe1.physicsBody = SKPhysicsBody(rectangleOf: bottomPipe1.frame.size)
        bottomPipe1.physicsBody?.categoryBitMask = PhysicsBody.Pipe
        bottomPipe1.physicsBody?.collisionBitMask = PhysicsBody.Bird
        bottomPipe1.physicsBody?.contactTestBitMask = PhysicsBody.Bird
        bottomPipe1.physicsBody?.affectedByGravity = false
        bottomPipe1.physicsBody?.isDynamic = false
        
        let randomPosition = CGFloat.random(min: -200, max: 200)
        PipePair.position.y = PipePair.position.y +  randomPosition
        
        PipePair.addChild(bottomPipe1)
        PipePair.addChild(topPipe1)
        PipePair.run(PipeMoveRemove)
        PipePair.zPosition = 2
        
        self.addChild(PipePair)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        SoundManager.prepareToPlay()
        SoundManager.play()
        
        if started == false{
        
            started = true
            start = true
            bird.physicsBody?.affectedByGravity = true
            bird.physicsBody?.velocity = CGVector.init(dx: 0, dy: 0)
            bird.physicsBody?.applyImpulse(CGVector.init(dx: 0, dy: 50))
            
             spawn = SKAction.run({
                
                () in
                self.createPipes()
                
            })
            
            let delay = SKAction.wait(forDuration: 2.00)
            
            let PipeSequence = SKAction.sequence([spawn,delay])
            
            let Pipeforever = SKAction.repeatForever(PipeSequence)
            
            self.run(Pipeforever)
            
            let distance  = CGFloat(self.frame.width + PipePair.frame.width)
            
            let movePipes = SKAction.moveBy(x: -distance, y: 0, duration: TimeInterval(distance * 0.01))
            
            let RemovePipes = SKAction.removeFromParent()
            
            PipeMoveRemove = SKAction.sequence([movePipes,RemovePipes])
            
            StartScoring()
        
        }
        else if(died){
            
            myLabel.removeFromParent()
            scoreLabel.removeFromParent()
            StartScene()
           
        }
        else{
            if(self.died)
            {
            }
            else
            {
                self.bird.physicsBody?.velocity = CGVector.init(dx: 0, dy: 0)
                self.bird.physicsBody?.applyImpulse(CGVector.init(dx: 0, dy: 50))
            }
        }
    }
    
    

func didBegin(_ contact: SKPhysicsContact) {
        
        let bodyA = contact.bodyA
        let bodyB = contact.bodyB
        
        if((bodyA.collisionBitMask == bird.physicsBody?.collisionBitMask && bodyB.collisionBitMask == myFloor1.physicsBody?.collisionBitMask) || (bodyA.collisionBitMask == myFloor1.physicsBody?.collisionBitMask && bodyB.collisionBitMask == bird.physicsBody?.collisionBitMask))
        {
            died = true
            self.removeAllActions()
            PipePair.removeAllActions()
            bird.removeAllActions()
            bird.removeFromParent()
            PipePair.removeFromParent()
            myLabel = SKLabelNode(fontNamed: "Arial")
            myLabel.text = "Game Over"
            myLabel.fontSize = 40
            myLabel.color = SKColor(red: 255, green: 255, blue: 255, alpha: 0)
            myLabel.position = CGPoint(x:self.frame.midX, y:self.frame.midY)
            myLabel.zPosition = 7
            self.addChild(myLabel)
            
        }
    }
   

}
