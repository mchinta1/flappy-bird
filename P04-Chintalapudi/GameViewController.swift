//
//  GameViewController.swift
//  P04-Chintalapudi
//
//  Created by MaNi on 2/28/17.
//  Copyright © 2017 MaNi. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation



class GameViewController: UIViewController {
    
    @IBOutlet weak var Titlelbl : UILabel!
    @IBOutlet weak var Easylbl : UIButton!
    @IBOutlet weak var Mediumlbl : UIButton!
    @IBOutlet weak var Hardlbl : UIButton!

    var backgroundImage = UIImageView()
override func viewDidLoad() {
    
   
     backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    backgroundImage.image = UIImage(named: "background3")
    self.view.insertSubview(backgroundImage, at: 0)
    
    
    super.viewDidLoad()
    
    
    }

    
    @IBAction func EasyGameStart(sender:AnyObject)
    {
    
        if let view = self.view as! SKView? {
            
            backgroundImage.removeFromSuperview()
            Titlelbl.removeFromSuperview()
            
            Easylbl.removeFromSuperview()
            Mediumlbl.removeFromSuperview()
            Hardlbl.removeFromSuperview()
            
            // Load the SKScene from 'GameScene.sks'
            let game = GameScene(size:view.bounds.size)
            
            game.size = view.bounds.size
            
            game.GameLevel = 1
            
            do{
                
                game.SoundManager = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath:Bundle.main.path(forResource: "touch" , ofType: "wav" )!))
            }
            catch
            {
                
                print(error)
                
            }
            
            view.presentScene(game)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }

    }
    
    @IBAction func MedGameStart(sender:AnyObject)
    {
        
        if let view = self.view as! SKView? {
            
             backgroundImage.removeFromSuperview()
            Titlelbl.removeFromSuperview()
            
            Easylbl.removeFromSuperview()
            Mediumlbl.removeFromSuperview()
            Hardlbl.removeFromSuperview()
            
            // Load the SKScene from 'GameScene.sks'
            let game = GameScene(size:view.bounds.size)
            
            game.size = view.bounds.size
            
            game.GameLevel = 2
            
            view.presentScene(game)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
        
    }
    
    @IBAction func HardGameStart(sender:AnyObject)
    {
        
        if let view = self.view as! SKView? {
            
             backgroundImage.removeFromSuperview()
            Titlelbl.removeFromSuperview()
            
            Easylbl.removeFromSuperview()
            Mediumlbl.removeFromSuperview()
            Hardlbl.removeFromSuperview()
            
            // Load the SKScene from 'GameScene.sks'
            let game = GameScene(size:view.bounds.size)
            
            game.size = view.bounds.size
            
            game.GameLevel = 3
            
            view.presentScene(game)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
        
    }
    
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
      /*  if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        } */
        return .portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
